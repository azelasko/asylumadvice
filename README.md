# Deploy to heroku

- run "mvn package -Pproduction -Dmaven.test.skip=true"
- run in target folder "heroku deploy:jar asylumadvice-1.0-SNAPSHOT-spring-boot.jar --app pure-shelf-89004"
- open https://pure-shelf-89004.herokuapp.com/availablelocalechoose

# Open h2 console

- Set JDBC Url to "jdbc:h2:mem:testdb"

# Start mysql docker container

docker run  --name mysql -e MYSQL_ROOT_PASSWORD=password -e MYSQL_DATABASE=asylumadvice -p 3306:3306 -d mysql
