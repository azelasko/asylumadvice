package de.arnzel.views.survey;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Input;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.router.Route;
import de.arnzel.views.AbstractMainView;


@Route
public class SurveyView extends AbstractMainView {

  @Override
  protected Component getContent() {
    VerticalLayout verticalLayout = new VerticalLayout();
    
    verticalLayout.add(new H1("Was the overview helpful ? 5 being most helpful"));
    RadioButtonGroup<Integer> ratingOverviewHelpful = new RadioButtonGroup<>();
    ratingOverviewHelpful.setItems(1,2,3,4,5);
    verticalLayout.add(ratingOverviewHelpful);

    verticalLayout.add(new H1("What other topics would you like to add ?"));
    verticalLayout.add(new TextArea());

    verticalLayout.add(new H1("Was the content easy to understand ? 5 being most easy"));
    RadioButtonGroup<Integer> contentUnderstandable = new RadioButtonGroup<>();
    contentUnderstandable.setItems(1,2,3,4,5);
    verticalLayout.add(contentUnderstandable);

    verticalLayout.add(new H1("Did you find the contract you need ?"));
    RadioButtonGroup<Boolean> contentFound = new RadioButtonGroup<>();
    contentFound.setItems(Boolean.TRUE,Boolean.FALSE);
    verticalLayout.add(contentFound);

    verticalLayout.add(new H1("Are there other services you would like to add ?"));
    verticalLayout.add(new TextArea());
    
    verticalLayout.add(new H1("Your email adress"));
    verticalLayout.add(new Input());

    return verticalLayout;
  }

  
}
