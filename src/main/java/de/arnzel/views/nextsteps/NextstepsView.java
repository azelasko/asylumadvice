package de.arnzel.views.nextsteps;

import static de.arnzel.legaladvice.LegalAdviceSessionHolder.getLegalAdviceSelected;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import de.arnzel.components.buttons.AsylumAdviceButtonRouting;
import de.arnzel.views.AbstractMainView;
import de.arnzel.legaladvice.LegalAdvice;
import de.arnzel.legaladvice.LegalAdviceInformationComponent;

@Route
public class NextstepsView extends AbstractMainView {

  @Override
  protected Component getContent() {
    VerticalLayout verticalLayout = new VerticalLayout();
    verticalLayout.add(new H1("Next Steps"));
    verticalLayout.add(new Paragraph("These steps will help yourself to prepare  for your appoitment with a legal advisor"));
    
    verticalLayout.add(new H2("Step 1:"));
    verticalLayout.add(new Paragraph("Download your overview and go through the information provided there. Also keep your answers to the guestionaire handy so you can show them or send them to your legal advisor"));
    verticalLayout.add(new AsylumAdviceButtonRouting("Download Overview",""));

    verticalLayout.add(new H2("Step 2:"));
    verticalLayout.add(new Paragraph("Gather the following materials: If possible gather the following materials: passport, marriage certificate... "));

    verticalLayout.add(new H2("Step 3:"));
    verticalLayout.add(new Paragraph("Choose legal advice place from the list you bookmarked"));

    // TODO: handle case when there is no legal advice in session
    LegalAdvice legalAdvice = getLegalAdviceSelected();
    verticalLayout.add(new LegalAdviceInformationComponent(legalAdvice));


    return verticalLayout;
  }

}
