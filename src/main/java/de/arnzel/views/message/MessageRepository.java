package de.arnzel.views.message;

import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface MessageRepository extends PagingAndSortingRepository<Message, Long> {

    @Override
    List<Message> findAll();


    Message findByLocaleAndKey(String locale,String key);
}
