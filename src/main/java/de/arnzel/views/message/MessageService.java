package de.arnzel.views.message;

import de.arnzel.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static de.arnzel.Session.getLocale;

@Component
public class MessageService {

    private final MessageRepository messageRepository;

    @Autowired
    public MessageService(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    public String getMessage(String key){
            return
                    messageRepository
                            .findByLocaleAndKey(getLocale(),key)
                    .getContent();
    }
}
