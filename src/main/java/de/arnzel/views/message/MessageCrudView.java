package de.arnzel.views.message;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.crudui.crud.impl.GridCrud;

@Route
public class MessageCrudView extends VerticalLayout {

    @Autowired
    public MessageCrudView(MessageRepository messageRepository) {
        GridCrud<Message> crud = new GridCrud<>(Message.class);
        setSizeFull();
        add(crud);
        crud.setOperations(
                () -> messageRepository.findAll(),
                legalAdvice -> messageRepository.save(legalAdvice),
                legalAdvice -> messageRepository.save(legalAdvice),
                legalAdvice -> messageRepository.delete(legalAdvice)
        );
    }
}
