package de.arnzel.views;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import de.arnzel.components.FooterNavbar;
import de.arnzel.components.bootstrap.Main;
import de.arnzel.components.bootstrap.Nav;
import de.arnzel.components.html.Button;
import de.arnzel.components.images.LogoImage;
import de.arnzel.availablelocale.AvailableLocaleChooseView;

import javax.annotation.PostConstruct;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@org.springframework.stereotype.Component
@CssImport("./styles/bootstrap/bootstrap.min.css")
@CssImport("./styles/fontAwesome/all.min.css")
public abstract class AbstractMainView extends VerticalLayout {

    @PostConstruct
    public void postConstruct(){
        setPadding(false);
        add(getHeaderNav());
        addContent();
        add(new FooterNavbar());
    }

    private Nav getHeaderNav(){
        Nav nav = new Nav();
        nav.getElement().getClassList().addAll(
                newArrayList("navbar",
                        "navbar-expand-lg",
                        "navbar-light",
                        "bg-light"));
        nav.add(getLeftNavbar());
        nav.add(getRightMenuButton());
        nav.add(getCollapseBar());
        nav.setWidth("100%");
        return nav;
    }

    private Component getCollapseBar() {
        Div collapseBar = new Div();
        collapseBar.getElement().getClassList().addAll(newArrayList("collapse",
                "navbar-collapse"));
        collapseBar.getElement().setAttribute("id","navbarSupportedContent");
        collapseBar.add(getCollapseBarLinks());
        return collapseBar;
    }

    private UnorderedList getCollapseBarLinks(){
        UnorderedList siteList = new UnorderedList();
        siteList
                .getElement()
                .getClassList()
                .addAll(newArrayList("navbar-nav",
                "mr-auto"));
        List<String> sites = newArrayList("Home",
                "Map",
                "My overview",
                "Next steps",
                "Concept",
                "Team",
                "Support",
                "Language");
        sites
                .stream()
                .forEach(site ->{
                    ListItem listItem = new ListItem();
                    listItem.getElement().getClassList().add("nav-item");
                    listItem.add(getCollapseBarLink(site));
                    siteList.add(listItem);
                } );
        return siteList;
    }

    private Anchor getCollapseBarLink(String text){
        Anchor anchor = new Anchor("#","");
        anchor.getElement().getClassList().addAll(newArrayList("text-center",
                "nav-link",
                "border-bottom"));
        Span span = new Span();
        span.getElement().getClassList().add("h4");
        span.setText(text);
        anchor.add(span);
        return anchor;
    }

    private Component getRightMenuButton() {
        //<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        Button button = new Button();
        button.getElement().getClassList().add("navbar-toggler");
        button.getElement().setAttribute("type","button");
        button.getElement().setAttribute("data-toggle","collapse");
        button.getElement().setAttribute("data-target","#navbarSupportedContent");
        button.getElement().setAttribute("aria-controls","navbarSupportedContent");
        button.getElement().setAttribute("aria-expanded","false");
        button.getElement().setAttribute("aria-label","Toggle navigation");
        Span span = new Span();
        span.getElement().getClassList().add("navbar-toggler-icon");
        button.add(span);
        return button;

    }

    private Component getLeftNavbar(){
        Nav nav = new Nav();
        nav.getElement().getClassList().addAll(
                newArrayList("navbar",
                        "navbar-light",
                        "bg-light"));
        Anchor anchor = new Anchor("/availablelocalechoose");
        anchor.add(getLogoLink());
        nav.add(anchor);
        return nav;
    }

    private Component getLogoLink(){
        LogoImage logoImage = new LogoImage();
        logoImage.setWidth("32px");
        logoImage.setHeight("32px");
        logoImage.addClickListener(imageClickEvent -> UI.getCurrent().navigate(AvailableLocaleChooseView.class));
        return logoImage;
    }

    private void addContent(){
        Main main = new Main();
        Div row = new Div();
        row.getElement().getClassList().add("row");
        Component content = getContent();
        row.add(content);
        main.add(row);
        add(main);
    }

    protected abstract Component getContent();

}
