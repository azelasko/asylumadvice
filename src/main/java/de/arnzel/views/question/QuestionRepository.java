package de.arnzel.views.question;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Component;

@Component
public interface QuestionRepository extends PagingAndSortingRepository<Question, Long>,
    QueryByExampleExecutor<Question> {

  @Override
  List<Question> findAll();

  @Query(value = "SELECT q FROM Question q left  join fetch  q.options")
  Set<Question> findAllWithOptions();

}
