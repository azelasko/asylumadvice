package de.arnzel.views.question;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "QUESTIONS")
public class Question {

  @Id
  @GeneratedValue(strategy= GenerationType.IDENTITY)
  @Column(name = "ID")
  private Long id;

  @Column(name = "QUESTION_TEXT")
  private String questionText;

  @OneToMany(mappedBy = "question",fetch = FetchType.LAZY)
  private List<Option> options;

  public Question() {
    options = new ArrayList<>();
  }

  public String getQuestionText() {
    return questionText;
  }

  public void setQuestionText(String questionText) {
    this.questionText = questionText;
  }

  public List<Option> getOptions() {
    return options;
  }

  public void setOptions(List<Option> options) {
    this.options = options;
  }
}
