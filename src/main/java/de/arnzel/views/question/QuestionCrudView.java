package de.arnzel.views.question;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.crudui.crud.impl.GridCrud;

@Route
public class QuestionCrudView extends VerticalLayout {

  @Autowired
  public QuestionCrudView(QuestionRepository questionRepository){
    GridCrud<Question> crud = new GridCrud<>(Question.class);
    setSizeFull();
    add(crud);
    crud.setOperations(
        () -> questionRepository.findAll(),
        question -> questionRepository.save(question),
        question -> questionRepository.save(question),
        question -> questionRepository.delete(question)
    );
  }

}
