package de.arnzel.views.question;

import de.arnzel.asyluminformation.AsylumInformation;

import javax.persistence.*;

@Entity
@Table(name = "OPTIONS")
public class Option {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "CODE")
    private String code;

    @Column(name = "TEXT")
    private String text;

    @ManyToOne(optional = false,fetch = FetchType.EAGER)
    @JoinColumn(name="QUESTION_ID", nullable=false)
    private Question question;

    @ManyToOne(optional = false,fetch = FetchType.EAGER)
    @JoinColumn(name="ASYLUM_INFORMATION_ID", nullable=false)
    private AsylumInformation asylumInformation;

    public Option() {
    }

    public Option(String text) {
        this.text = text;
    }
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public AsylumInformation getAsylumInformation() {
        return asylumInformation;
    }

    public void setAsylumInformation(AsylumInformation asylumInformation) {
        this.asylumInformation = asylumInformation;
    }
}
