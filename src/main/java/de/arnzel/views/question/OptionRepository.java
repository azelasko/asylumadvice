package de.arnzel.views.question;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface OptionRepository extends PagingAndSortingRepository<Option, Long> {

    List<Option> findByQuestion(Question q);
}
