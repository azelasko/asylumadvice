package de.arnzel.views.overview;

import static com.google.common.collect.Sets.newHashSet;
import static de.arnzel.legaladvice.LegalAdviceSessionHolder.*;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.router.Route;
import de.arnzel.components.buttons.AsylumAdviceButton;
import de.arnzel.components.buttons.AsylumAdviceButtonRouting;
import de.arnzel.components.images.PathImage;


import de.arnzel.legaladvice.LegalAdvice;
import de.arnzel.legaladvice.searchresults.LegalAdviceSearchResultsView;
import de.arnzel.views.AbstractMainView;
import de.arnzel.views.question.Option;
import de.arnzel.views.question.OptionRepository;
import de.arnzel.views.question.Question;
import de.arnzel.views.question.QuestionRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

@Route
public class ShowQuestionsView extends AbstractMainView {

  private final QuestionRepository questionRepository;

  private final OptionRepository optionRepository;

  private final List<Select<Option>> selects = new ArrayList<>();

  @Autowired
  public ShowQuestionsView(QuestionRepository questionRepository,
                           OptionRepository optionRepository){
    this.questionRepository = questionRepository;
    this.optionRepository = optionRepository;
  }

  @Override
  protected Component getContent() {
    
    VerticalLayout layout = new VerticalLayout();
    PathImage questionnaireImage = new PathImage("images/questionnaire/show/overview.png");
    questionnaireImage.setWidth("100%");
    questionnaireImage.setHeight("auto");
    layout.add(questionnaireImage);
    
    H1 h1 = new H1("Get an overview of you asylum procedure");
    layout.add(h1);

    Paragraph paragraph = new Paragraph();
    paragraph.setText("By answering the following questions Asylum Advice can give you a general overview of what will be important in your asylum procedure and where you can find further help, If you can't answer one of the questions or feel uncomfortable to do so, ");
    layout.add(paragraph);

    layout.add(new Hr());
    
    addQuestionComponents(layout);
    layout.add(getRoutingButtonOverview());
    return layout;
  }

  private Component getRoutingButtonOverview(){
    AsylumAdviceButton asylumAdviceButton = new AsylumAdviceButton("Get Overview", event -> {
        setSelectedOptions(selects.stream().map(select-> select.getValue()).collect(Collectors.toList()));
        UI.getCurrent().navigate(ShowOverviewView.class);
    });
    return asylumAdviceButton;
  }
  
  private void addQuestionComponents(VerticalLayout layout){
    Collection<Question> questions = questionRepository.findAllWithOptions();
    questions.stream().forEach(question->{
      Select<Option> select = new Select<>();
      layout.add(new Label(question.getQuestionText()));
      select.setItemLabelGenerator(Option::getText);
      select.setWidth("100%");
      select.setItems(question.getOptions());
      selects.add(select);
      layout.add(select);
    });
    TextField textField = new TextField();
    textField.setLabel("In which location are you looking for\n" +
            "legal aid?");
    textField.setWidth("100%");
    layout.add(textField);
  }

}
