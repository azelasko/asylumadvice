package de.arnzel.views.overview;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import de.arnzel.asyluminformation.AsylumInformation;
import de.arnzel.asyluminformation.AsylumInformationView;
import de.arnzel.components.buttons.AsylumAdviceButtonRouting;
import de.arnzel.components.images.PathImage;
import de.arnzel.legaladvice.LegalAdviceSessionHolder;
import de.arnzel.views.AbstractMainView;
import de.arnzel.asyluminformation.AsylumInformationRepository;
import de.arnzel.views.nextsteps.NextstepsView;
import de.arnzel.views.question.Option;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Route
public class ShowOverviewView extends AbstractMainView {

  private final AsylumInformationRepository asylumInformationRepository;

  @Autowired
  public ShowOverviewView(AsylumInformationRepository asylumInformationRepository) {
    this.asylumInformationRepository = asylumInformationRepository;
  }

  @Override
  protected Component getContent() {
    VerticalLayout layout = new VerticalLayout();
    
    PathImage questionnaireImage = new PathImage("images/questionnaire/show/overview.png");
    questionnaireImage.setWidth("100%");
    questionnaireImage.setHeight("auto");
    layout.add(questionnaireImage);
    
    H1 h1 = new H1("Your Overview");
    layout.add(h1);

    Paragraph paragraph = new Paragraph("In the following we will show you information about topics in the asylum procedure that are important to your individual case. Try to read about the topics before you see a legal advisor. It will help you to be more informed.");
    layout.add(paragraph);
    
    layout.add(new Hr());
    
    addAsylumInformationComponents(layout);
    
    return layout;
  }

  private void addAsylumInformationComponents(VerticalLayout layout) {
    getAsylumInformations()
        .stream()
        .forEach(asylumInformation -> {
          layout.add(new H2(asylumInformation.getTitle()));
          layout.add(new Paragraph(asylumInformation.getExplanationText()));
          layout.add(new AsylumAdviceButtonRouting("Read More",
                  AsylumInformationView.class,asylumInformation.getId()));
          layout.add(new Hr());
        });
    layout.add(new AsylumAdviceButtonRouting("Next steps", NextstepsView.class));
  }

  private List<AsylumInformation> getAsylumInformations() {
    List<AsylumInformation> asylumInformations = new ArrayList<>();
    for (Option option :
            LegalAdviceSessionHolder.getOptionsSelected()) {
      if (null != option) {
        AsylumInformation asylumInformation =
                option.getAsylumInformation();
        if (null != asylumInformation) {
          asylumInformations.add(asylumInformation);
        }
      }
    }
    return asylumInformations;
  }
}
