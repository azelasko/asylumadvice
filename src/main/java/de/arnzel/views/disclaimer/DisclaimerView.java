package de.arnzel.views.disclaimer;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import de.arnzel.components.buttons.AsylumAdviceButtonRouting;
import de.arnzel.views.AbstractMainView;
import de.arnzel.legaladvice.search.LegalAdviceSearchView;
import de.arnzel.views.message.MessageService;
import org.springframework.beans.factory.annotation.Autowired;

@Route
public class DisclaimerView extends AbstractMainView {

  private final MessageService messageService;

  @Autowired
  public DisclaimerView(MessageService messageService){
    this.messageService = messageService;
  }

  @Override
  protected Component getContent() {
    VerticalLayout verticalLayout = new VerticalLayout();
    verticalLayout.setJustifyContentMode(JustifyContentMode.BETWEEN);
    verticalLayout.add(new H1("Disclaimer"));
    verticalLayout.add(new Paragraph("Asylum Advice is making access to legal  advice for asylum seekers more accessible and helps with getting a better orientation in the landscape of the asylum procedure. The service does not provide legal advice itself. "));
    verticalLayout.add(new Paragraph("In the following you agree to take the content of Asylum Advice only as a basis for actual legal aid that you will find in our search engine. "));
    verticalLayout.add(new Paragraph("I agree to the terms of Asylum Advice by clicking proceed. See further details here."));

    verticalLayout.add(new AsylumAdviceButtonRouting("Agree and Continue", LegalAdviceSearchView.class));
    
    return verticalLayout;
  }

}
