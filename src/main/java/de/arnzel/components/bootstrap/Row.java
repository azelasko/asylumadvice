package de.arnzel.components.bootstrap;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.Tag;

@Tag("row")
public class Row extends Component implements HasComponents {

    public Row(){
        getElement().getClassList().add("row");
    }

    public void setPaddingBottom(String paddingBottom){
        getElement().getStyle().set("padding-bottom",paddingBottom);
    }
}
