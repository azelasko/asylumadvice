package de.arnzel.components.bootstrap;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.Tag;

@Tag("container")
public class Container extends Component implements HasComponents {

    public Container(){
        getElement().getClassList().add("container");
    }
}
