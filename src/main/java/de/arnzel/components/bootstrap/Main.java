package de.arnzel.components.bootstrap;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.Tag;

@Tag("main")
public class Main extends Component implements HasComponents {

    public Main(){
        getElement().setAttribute("role","main");
        getElement().getClassList().add("container");
        getElement().getStyle().set("padding-bottom","25vh");

    }
}
