package de.arnzel.components.bootstrap;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.Tag;

@Tag("col8")
public class Col8  extends Component implements HasComponents {

    public Col8() {
        getElement().getClassList().add("col-8");
    }
}
