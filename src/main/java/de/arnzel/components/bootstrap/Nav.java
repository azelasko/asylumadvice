package de.arnzel.components.bootstrap;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.Tag;

@Tag("nav")
public class Nav extends Component implements HasComponents, HasSize {
}
