package de.arnzel.components.bootstrap;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Tag;

@Tag("col2")
public class Col2 extends Component {

    public Col2() {
        getElement().getClassList().add("col-2");
    }
}
