package de.arnzel.components.images;

import static org.apache.commons.lang3.StringUtils.substringAfterLast;

import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.server.StreamResource;
import de.arnzel.components.ExtendedComponentFunctions;
import java.io.InputStream;

public class PathImage extends Image implements ExtendedComponentFunctions {

  public PathImage(String path){
    setSrc(getStreamResource(path));
    setAlt(substringAfterLast(path,"/"));
  }

  public void centerImage(String width){
    centerComponent(width);
  }
  
  public void centerImage(){
    centerImage("50%");
  }
  
  private  StreamResource getStreamResource(String path){
    return new StreamResource(substringAfterLast(path,"/"),
        () -> getImage(path));
  }

  private  InputStream getImage(String path)  {
      return getClass().getClassLoader().getResourceAsStream(path);
  }

}
