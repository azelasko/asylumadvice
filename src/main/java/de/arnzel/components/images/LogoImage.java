package de.arnzel.components.images;

public class LogoImage extends PathImage {

  public LogoImage() {
    super("images/asylumAdviceLogo.svg");
  }

  public LogoImage(String width) {
    this();
    setWidth(width);
  }
}
