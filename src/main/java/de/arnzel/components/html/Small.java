package de.arnzel.components.html;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasText;
import com.vaadin.flow.component.Tag;

@Tag("small")
public class Small extends Component implements HasText {

    public Small(String text){
        setText(text);
    }
}
