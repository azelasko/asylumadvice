package de.arnzel.components;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Anchor;
import de.arnzel.components.bootstrap.Nav;
import de.arnzel.components.images.LogoImage;
import de.arnzel.availablelocale.AvailableLocaleChooseView;

import static com.google.common.collect.Lists.newArrayList;

@Tag("headernav")
public class HeaderNavbar extends Component implements HasComponents {

    public HeaderNavbar(){
        getElement().getClassList().addAll(
                newArrayList("navbar",
                        "navbar-expand-lg",
                        "navbar-light",
                        "fixed-top",
                        "bg-light"));
        add(getLeftNavbar());
    }

    private Component getLeftNavbar(){
        Nav nav = new Nav();
        nav.getElement().getClassList().addAll(
                newArrayList("navbar",
                        "navbar-light",
                        "bg-light"));
        Anchor anchor = new Anchor("#","");
        anchor.add(getLogoLink());
        nav.add(anchor);
        return nav;
    }

    private Component getLogoLink(){
            LogoImage logoImage = new LogoImage();
            logoImage.setWidth("32px");
            logoImage.setHeight("32px");
            logoImage.addClickListener(imageClickEvent -> UI.getCurrent().navigate(AvailableLocaleChooseView.class));
            return logoImage;
    }
}
