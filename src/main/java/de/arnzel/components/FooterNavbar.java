package de.arnzel.components;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Div;
import de.arnzel.components.html.Italic;

import static com.google.common.collect.Lists.newArrayList;

@Tag("footerNavbar")
public class FooterNavbar extends Component implements HasComponents {

    public FooterNavbar(){
       getElement().getClassList().addAll(newArrayList("navbar",
                "fixed-bottom","navbar-light","bg-light"));
        //TODO: complete footer
        add(new Html("<p>\n" +
                "\t\t\t\t<small>©Asylum Advice 2020\n" +
                "\t\t\t\t<br>\n" +
                "\t\t\t\tPrivacy policy\n" +
                "\t\t\t\t<br>\n" +
                "\t\t\t\tcontact@asylumadvice.org\n" +
                "\t\t\t\t</small>\n" +
                "\t\t\t</p>"));
        Div div = new Div();
        Anchor facebookLink
                = new Anchor("https://www.facebook.com/Asylum-Advice-327356624875158",
                "Follow us on Facebook");
        Italic faceBookLogo = new Italic();
        faceBookLogo.getElement()
                .getClassList()
                .addAll(newArrayList("fab","fa-facebook-square"));
        div.add(faceBookLogo);
        div.add(facebookLink);
        add(div);
    }
}
