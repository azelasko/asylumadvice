package de.arnzel.components;

import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.HasStyle;

public interface ExtendedComponentFunctions extends HasStyle, HasSize {

  default void centerComponent(String width){
    this.getStyle().set("display","block");
    getStyle().set("margin-left","auto");
    getStyle().set("margin-right","auto");
    setWidth(width);
  }

  default void setNoPadding(){
    this.getStyle().set("padding","0 0 0 0");
  }
  
}
