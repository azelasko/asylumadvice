package de.arnzel.components;

import static com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment.CENTER;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Label;
import de.arnzel.components.images.PathImage;
import de.arnzel.components.layouts.ExtendedHorizontalLayout;

public class Footer  extends ExtendedHorizontalLayout {

  public Footer(){

    Label asylumAdviceLabel =
        new Label("@Asylum Advice 2020");
    setFlexGrow(8,asylumAdviceLabel);
    setVerticalComponentAlignment(CENTER,
        asylumAdviceLabel);

    Anchor facebookLinkImage = new Anchor("https://www.facebook.com/Asylum-Advice-327356624875158");
    PathImage facebookImage =
        new PathImage("images/facebook.svg");
    facebookImage.setWidth("10%");
    facebookLinkImage.add(facebookImage);
    setFlexGrow(1,facebookLinkImage);



    Label followUsLabel = new Label("Follow us on Facebook");
    setFlexGrow(1,followUsLabel);
    setVerticalComponentAlignment(CENTER,
        followUsLabel);
    
    add(asylumAdviceLabel);
    add(facebookLinkImage);
    add(followUsLabel);

    setHeight("40px");
  }
  
}
