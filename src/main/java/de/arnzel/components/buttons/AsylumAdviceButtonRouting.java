package de.arnzel.components.buttons;

import static com.vaadin.flow.component.UI.getCurrent;

import com.vaadin.flow.component.Component;

public class AsylumAdviceButtonRouting extends AsylumAdviceButton {

  public AsylumAdviceButtonRouting(String text, Class<? extends Component> viewClass){
    super(text);
    addClickListener(event -> {
      getCurrent().navigate(viewClass);
    });
  }

  public AsylumAdviceButtonRouting(String text, Class viewClass,Object parameter){
    super(text);
    addClickListener(event -> {
      getCurrent().navigate(viewClass,parameter);
    });
  }

  public AsylumAdviceButtonRouting(String text, String path){
    super(text);
    addClickListener(event -> {
      getCurrent().navigate(path);
    });
  }

}
