package de.arnzel.components.buttons;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import de.arnzel.components.ExtendedComponentFunctions;
import de.arnzel.components.html.Button;

import static com.google.common.collect.Lists.newArrayList;

public class AsylumAdviceButton extends Button implements ExtendedComponentFunctions {

  private static final String WIDTH = "100%";

  private static final String HEIGHT = "75px";
  
  public AsylumAdviceButton(String text){
    getElement().getClassList().addAll(newArrayList("btn",
            "btn-info","btn-lg","btn-block"));
    setText(text);
    centerImage();
    setHeight(HEIGHT);
  }
  
  public AsylumAdviceButton(String text, ComponentEventListener<ClickEvent<Button>> clickListener){
    this(text);
    this.addClickListener(clickListener);
  }

  private void centerImage(){
    centerComponent(WIDTH);
  }
  
}
