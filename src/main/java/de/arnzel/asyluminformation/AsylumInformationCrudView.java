package de.arnzel.asyluminformation;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.crudui.crud.impl.GridCrud;

@Route
public class AsylumInformationCrudView extends VerticalLayout {
  
  @Autowired
  public AsylumInformationCrudView(AsylumInformationRepository asylumInformationRepository){
    GridCrud<AsylumInformation> crud = new GridCrud<>(AsylumInformation.class);
    setSizeFull();
    add(crud);
    crud.setOperations(
        () -> asylumInformationRepository.findAll(),
        asylumInformation -> asylumInformationRepository.save(asylumInformation),
        asylumInformation -> asylumInformationRepository.save(asylumInformation),
        asylumInformation -> asylumInformationRepository.delete(asylumInformation)
    );
  }
}
