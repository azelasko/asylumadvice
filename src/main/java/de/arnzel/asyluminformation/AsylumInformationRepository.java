package de.arnzel.asyluminformation;

import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface AsylumInformationRepository extends PagingAndSortingRepository<AsylumInformation, Long>,
    QueryByExampleExecutor<AsylumInformation> {

  @Override
  List<AsylumInformation> findAll();
}
