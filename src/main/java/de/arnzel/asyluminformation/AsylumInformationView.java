package de.arnzel.asyluminformation;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.HtmlComponent;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import de.arnzel.components.buttons.AsylumAdviceButtonRouting;
import de.arnzel.components.layouts.ExtendedVerticalLayout;
import de.arnzel.legaladvice.LegalAdvice;
import de.arnzel.legaladvice.LegalAdviceInformationComponent;
import de.arnzel.views.AbstractMainView;
import de.arnzel.views.overview.ShowQuestionsView;

import java.util.Optional;

@Route
public class AsylumInformationView extends AbstractMainView implements HasUrlParameter<Long> {

    private final AsylumInformationRepository asylumInformationRepository;

    private Optional<AsylumInformation> asylumInformationOptional;

    private ExtendedVerticalLayout verticalLayout = new ExtendedVerticalLayout();

    public AsylumInformationView(AsylumInformationRepository asylumInformationRepository) {
        this.asylumInformationRepository = asylumInformationRepository;
    }

    @Override
    protected Component getContent() {
        return verticalLayout;
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, Long id) {
        asylumInformationOptional = asylumInformationRepository
                .findById(id);
        if(asylumInformationOptional.isPresent()){
            AsylumInformation asylumInformation = asylumInformationOptional
                    .get();
            verticalLayout.add(new Html("<div>" + asylumInformation.getExplanationTextLong() + "<div>"));
        }else {
            verticalLayout.add(new Label("The article about asylum information does not exists anymore"));
        }
    }
}
