package de.arnzel.asyluminformation;

import de.arnzel.views.question.Option;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "ASYLUM_INFORMATION")
public class AsylumInformation {

  @Id
  @GeneratedValue(strategy= GenerationType.IDENTITY)
  @Column(name = "ID")
  private Long id;

  @Column(name = "TITLE")
  private String title;

  @Column(name = "CODE")
  private String code;

  @Column(columnDefinition="text",name = "EXPLANATION_TEXT")
  private String explanationText;

  @Column(columnDefinition="mediumText",name = "EXPLANATION_TEXT_LONG")
  private String explanationTextLong;

  @Column(name = "PATH")
  private String path;

  @OneToMany(mappedBy = "asylumInformation",fetch = FetchType.EAGER)
  private List<Option> options;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getExplanationText() {
    return explanationText;
  }

  public void setExplanationText(String explanationText) {
    this.explanationText = explanationText;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public List<Option> getOptions() {
    return options;
  }

  public void setOptions(List<Option> options) {
    this.options = options;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getExplanationTextLong() {
    return explanationTextLong;
  }

  public void setExplanationTextLong(String explanationTextLong) {
    this.explanationTextLong = explanationTextLong;
  }

  public Long getId() {
    return id;
  }
}
