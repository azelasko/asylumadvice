package de.arnzel;

import de.arnzel.asyluminformation.AsylumInformation;

import java.util.Set;

import static com.vaadin.flow.server.VaadinSession.getCurrent;

public class Session {

    private static final String SESSION_LOCALE_KEY = "locale";

    private static final String SESSION_ASYLUM_INFORMATIONS_KEY = "asylumInformations";


    public static void setLocale(String locale){
        getCurrent()
                .setAttribute(SESSION_LOCALE_KEY,locale);
    }

    public static String getLocale(){
        return (String)getCurrent()
                .getAttribute(SESSION_LOCALE_KEY);
    }

    public static void setAsylumInformations(Set<AsylumInformation> asylumInformations){
        getCurrent().setAttribute(SESSION_ASYLUM_INFORMATIONS_KEY,asylumInformations);
    }

    public Set<AsylumInformation> getAsylumInformations(){
        return (Set<AsylumInformation>)getCurrent()
                .getAttribute(SESSION_ASYLUM_INFORMATIONS_KEY);
    }
}
