package de.arnzel.service;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import de.arnzel.legaladvice.LegalAdvice;
import org.springframework.stereotype.Component;

import java.io.IOException;

import static de.arnzel.Config.GOOGLE_MAPS_API_KEY;

@Component
public class GeocodingService {

    public LatLng getLatLon(LegalAdvice legalAdvice) throws InterruptedException, ApiException, IOException {
        GeoApiContext context = new GeoApiContext.Builder()
                .apiKey(GOOGLE_MAPS_API_KEY)
                .build();
        GeocodingResult[] results =  GeocodingApi.geocode(context,
                getAddress(legalAdvice)).await();
        return results[0].geometry.location;
    }

    private String getAddress(LegalAdvice legalAdvice){
        return legalAdvice.getStreet()
                + " "
                + legalAdvice.getCity()
                + ","
                + legalAdvice.getPostalCode()
                + " "
                + legalAdvice.getCity();
    }
}
