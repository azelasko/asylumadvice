package de.arnzel.availablelocale;

import javax.persistence.*;

@Entity
@Table(name = "AVAILABLE_LOCALES")
public class AvailableLocale {

  @Id
  @GeneratedValue(strategy= GenerationType.IDENTITY)
  @Column(name = "ID")
  private Long id;

  @Column(name = "LOCALE")
  private String locale;

  @Column(name = "DISPLAY_TEXT")
  private String localeDisplayText;

  public void setLocale(String locale) {
    this.locale = locale;
  }

  public void setLocaleDisplayText(String localeDisplayText) {
    this.localeDisplayText = localeDisplayText;
  }

  public String getLocaleDisplayText() {
    return localeDisplayText;
  }

  public String getLocale() {
    return locale;
  }
}
