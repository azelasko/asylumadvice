package de.arnzel.availablelocale;

import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import de.arnzel.components.FooterNavbar;
import de.arnzel.components.bootstrap.*;
import de.arnzel.components.images.LogoImage;
import org.springframework.beans.factory.annotation.Autowired;

@Route
@CssImport("./styles/bootstrap/bootstrap.min.css")
@CssImport("./styles/fontAwesome/all.min.css")
public class AvailableLocaleChooseView extends VerticalLayout {

    private final AvailableLocaleRepository availableLocaleRepository;

    @Autowired
    public AvailableLocaleChooseView(AvailableLocaleRepository availableLocaleRepository){
        this.availableLocaleRepository = availableLocaleRepository;
        add(getMain());
        add(new FooterNavbar());
    }

    private Main getMain(){
        Main main = new Main();
        Row row = new Row();
        Col2 left = new Col2();
        Col8 middle = getMiddleCol8();
        Col2 right = new Col2();
        row.add(left);
        row.add(middle);
        row.add(right);
        main.add(row);
        return main;
    }

    public Col8 getMiddleCol8(){
        Col8 middle = new Col8();
        Div content = new Div();
        LogoImage logoImage = new LogoImage();
        logoImage.getElement().getClassList().add("img-fluid");
        logoImage.getElement().getClassList().add("mx-auto");
        logoImage.getElement().getStyle().set("margin-bottom","8px");
        logoImage.setWidth("100%");
        content.add(logoImage);

        availableLocaleRepository
                .findAll()
                .stream()
                .forEach(locale -> {
                    content.add(new AvailableLocaleChooseButton(locale));
                });
        middle.add(content);
        return middle;
    }
}
