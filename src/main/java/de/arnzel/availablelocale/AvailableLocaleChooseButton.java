package de.arnzel.availablelocale;

import com.vaadin.flow.component.ClickNotifier;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasText;
import com.vaadin.flow.component.Tag;
import de.arnzel.views.disclaimer.DisclaimerView;

import static com.google.common.collect.Lists.newArrayList;
import static com.vaadin.flow.component.UI.getCurrent;
import static de.arnzel.Session.setLocale;

@Tag("chooseAvailableLocaleButton")
public class AvailableLocaleChooseButton extends Component implements HasText, ClickNotifier {

    public AvailableLocaleChooseButton(AvailableLocale availableLocale) {
        setText(availableLocale.getLocaleDisplayText());
        getElement().getClassList().addAll(newArrayList("btn",
                "btn-info","btn-lg","btn-block"));
        getElement().setAttribute("type","button");
        addClickListener(event -> {
            setLocale(availableLocale.getLocale().toString());
            getCurrent().navigate(DisclaimerView.class);
        });
    }
}
