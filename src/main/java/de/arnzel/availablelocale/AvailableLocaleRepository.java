package de.arnzel.availablelocale;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface AvailableLocaleRepository extends PagingAndSortingRepository<AvailableLocale, Long> {

    @Override
    List<AvailableLocale> findAll();

}
