package de.arnzel.availablelocale;

import java.util.Set;
import org.springframework.stereotype.Component;

@Component
public interface AvailableLocaleProvider {
  
  public Set<AvailableLocale> getAvailableLocales();

}
