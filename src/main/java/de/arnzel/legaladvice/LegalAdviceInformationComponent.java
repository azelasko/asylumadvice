package de.arnzel.legaladvice;

import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import de.arnzel.components.images.PathImage;

public class LegalAdviceInformationComponent extends VerticalLayout {
  
  public LegalAdviceInformationComponent(LegalAdvice legalAdvice){
    PathImage image = new PathImage("images/legalAdvice/show/LegalAdvisor.png");
    //image.setWidth("100%");
    //image.setHeight("auto");
    add(image);

    H1 h1 = new H1(legalAdvice.getName());
    add(h1);

    Label adressLabel = new Label(legalAdvice.getFullAdress());
    add(adressLabel);

    Label phoneLabel = new Label(legalAdvice.getTelephoneNumber());
    add(phoneLabel);

    Anchor emailLink = new Anchor("mailto:"+ legalAdvice.getEmail(),
            legalAdvice.getEmail());
    add(emailLink);

    Anchor websiteLink = new Anchor(null !=legalAdvice.getHomepage() ? legalAdvice.getHomepage() : "",legalAdvice.getHomepage());
    add(websiteLink);

    Label availabilityLabel = new Label(legalAdvice.getOpeningTimes());
    add(availabilityLabel);
    
    Label languageLabel = new Label(legalAdvice.getLanguagesString());
    add(languageLabel);

  }

}
