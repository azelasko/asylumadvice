package de.arnzel.legaladvice.crud;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import de.arnzel.legaladvice.LegalAdvice;
import de.arnzel.legaladvice.LegalAdviceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.crudui.crud.impl.GridCrud;

@Route
public class LegalAdviceCrudView extends VerticalLayout {

  @Autowired
  public LegalAdviceCrudView(LegalAdviceRepository legalAdviceRepository) {
    GridCrud<LegalAdvice> crud = new GridCrud<>(LegalAdvice.class);
    setSizeFull();
    add(crud);
    crud.setOperations(
        () -> legalAdviceRepository.findAll(),
        legalAdvice -> legalAdviceRepository.save(legalAdvice),
        legalAdvice -> legalAdviceRepository.save(legalAdvice),
        legalAdvice -> legalAdviceRepository.delete(legalAdvice)
    );
  }
}
