package de.arnzel.legaladvice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.Valid;

@Component
public class LegalAdviceService {

    private final LegalAdviceRepository legalAdviceRepository;

    @Autowired
    public LegalAdviceService(LegalAdviceRepository legalAdviceRepository) {
        this.legalAdviceRepository = legalAdviceRepository;
    }

    public void saveLegalAdvice(@Valid  LegalAdvice legalAdvice){
        legalAdviceRepository.save(legalAdvice);
    }

}
