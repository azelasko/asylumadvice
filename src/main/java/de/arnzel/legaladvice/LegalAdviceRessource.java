package de.arnzel.legaladvice;

import com.google.maps.errors.ApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController()
public class LegalAdviceRessource {

    private final LegalAdviceExcelImporter legalAdviceExcelImporter;

    @Autowired
    public LegalAdviceRessource(LegalAdviceExcelImporter legalAdviceExcelImporter) {
        this.legalAdviceExcelImporter = legalAdviceExcelImporter;
    }

    @GetMapping("/legalAdvice/importExcel")
    public void importExcelFile() throws InterruptedException, ApiException, IOException {
        legalAdviceExcelImporter.importLegalAdvicesExcelSheet();
    }
}
