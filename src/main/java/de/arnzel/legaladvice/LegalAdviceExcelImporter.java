package de.arnzel.legaladvice;

import com.google.common.collect.Sets;
import com.google.maps.errors.ApiException;
import com.google.maps.model.LatLng;
import de.arnzel.service.GeocodingService;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static com.google.common.collect.Sets.newHashSet;
import static java.util.Arrays.asList;
import static org.apache.commons.lang3.StringUtils.substringBefore;
import static org.apache.poi.ss.usermodel.CellType.NUMERIC;
import static org.apache.poi.ss.usermodel.CellType.STRING;

@Component
public class LegalAdviceExcelImporter {

    private final LegalAdviceService legalAdviceService;

    private final GeocodingService geocodingService;

    @Autowired
    public LegalAdviceExcelImporter(LegalAdviceService legalAdviceService,
                                    GeocodingService geocodingService){
        this.legalAdviceService = legalAdviceService;
        this.geocodingService = geocodingService;
    }

    public void importLegalAdvicesExcelSheet() throws IOException, ApiException, InterruptedException {
        File file = new File(getClass().getClassLoader().getResource("legalAdvices.xlsx").getFile());
        FileInputStream fip = new FileInputStream(file);
        XSSFWorkbook workbook = new XSSFWorkbook(fip);
        XSSFSheet sheet = workbook.getSheet("result");
        Iterator<Row> rows = sheet.rowIterator();
        // skip headline
        rows.next();
        while (rows.hasNext()){
            Row row = rows.next();
            System.out.println("Importing row number:"+ row.getRowNum());
            String name = row
                    .getCell(0)
                    .getStringCellValue();
            String city = row
                    .getCell(2)
                    .getStringCellValue();
            Cell streetCell = row
                    .getCell(3);
            if(null != streetCell){
                String street = streetCell.getStringCellValue();
                String postalCode = getPostalCode(row.getCell(5));
                Set<String> languages = getLanguages(row.getCell(9));
                String telephoneNumber = getTelephoneNumber(row.getCell(10));
                String email = getEmail(row.getCell(11));
                String homepage = getHomepage(row.getCell(13));
                String openingTimes = getOpeningHours(row.getCell(15));
                LegalAdvice legalAdvice = new LegalAdvice();
                legalAdvice.setName(name);
                legalAdvice.setCity(city);
                legalAdvice.setStreet(street);
                legalAdvice.setPostalCode(postalCode);
                legalAdvice.setTelephoneNumber(telephoneNumber);
                legalAdvice.setEmail(email);
                legalAdvice.setHomepage(homepage);
                legalAdvice.setOpeningTimes(openingTimes);
                legalAdvice.setSupportsFreeLegalAid(true);
                legalAdvice.setLanguages(languages);
                LatLng latLng = geocodingService.getLatLon(legalAdvice);
                legalAdvice.setLatitude(latLng.lat);
                legalAdvice.setLongtitude(latLng.lng);
                System.out.println("Save Legal Advice:" + legalAdvice.toString());
                legalAdviceService.saveLegalAdvice(legalAdvice);
            }

        }

    }

    private Set<String> getLanguages(Cell cell) {
        if(null == cell){
            return new HashSet<>();
        }else{
            CellType cellType = cell.getCellTypeEnum();
            if(cellType.equals(STRING)){
                String cellString = cell.getStringCellValue();
                return
                        newHashSet(cellString.split(","))
                        .stream()
                        .map(s -> new String(s.trim()))
                        .collect(Collectors.toSet());
            }
            else  return new HashSet<>();
        }
    }

    private String getOpeningHours(Cell cell){
        if(null == cell){
            return "No opening hours";
        }else{
            CellType cellType = cell.getCellTypeEnum();
            if(cellType.equals(STRING)){
                return  cell.getStringCellValue();
            }else return "No opening hours";
        }
    }

    private String getHomepage(Cell cell){
        if(null == cell){
            return null;
        }else{
            CellType cellType = cell.getCellTypeEnum();
            if(cellType.equals(STRING)){
                return cell.getStringCellValue();
            }else return null;
        }
    }

    private String getEmail(Cell cell){
        if(null == cell){
            return null;
        }else{
            CellType cellType = cell.getCellTypeEnum();
            if(cellType.equals(STRING)){
                return  cell.getStringCellValue();
            }else return null;
        }
    }

    private String getTelephoneNumber(Cell cell) {
        if(null == cell){
            return "No TelephoneNumber";
        }else{
            CellType cellType = cell.getCellTypeEnum();
            if(cellType.equals(STRING)){
                return  cell.getStringCellValue();
            }else return "No TelephoneNumber";
        }
    }

    private String getPostalCode(Cell cell){
        CellType cellType = cell.getCellTypeEnum();
        if(cellType.equals(STRING)){
            return  substringBefore(cell.getStringCellValue()," ");
        }else if(cellType.equals(NUMERIC)){
            return "" + cell.getNumericCellValue();
        }else throw new RuntimeException("Unexpected cell type:"
                + cellType);
    }
}
