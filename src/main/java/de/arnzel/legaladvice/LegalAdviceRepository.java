package de.arnzel.legaladvice;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Component;

@Component
public interface LegalAdviceRepository extends PagingAndSortingRepository<LegalAdvice, Long>,
    QueryByExampleExecutor<LegalAdvice> {

  @Override
  List<LegalAdvice> findAll();

  @Query("SELECT DISTINCT city FROM LegalAdvice order by city")
  List<String> findAllLegalAdviceCities();


  @Query(
          value = "SELECT DISTINCT language FROM LEGAL_ADVICE_LANGUAGES ORDER BY language",
          nativeQuery = true)
  List<String> findAllLegalAdviceLanguages();

  @Query(value = "Select  l from LegalAdvice l WHERE l.city =:city AND (:firstLanguage MEMBER OF l.languages OR :secondLanguage MEMBER OF l.languages)")
  List<LegalAdvice> findByCityByCityAndLanguages(String city,String firstLanguage,String secondLanguage);
}
