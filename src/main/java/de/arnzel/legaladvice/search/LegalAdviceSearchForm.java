package de.arnzel.legaladvice.search;

import static com.vaadin.flow.component.UI.getCurrent;
import static de.arnzel.legaladvice.LegalAdviceSessionHolder.setLegalAdviceSearchResults;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import de.arnzel.components.buttons.AsylumAdviceButton;
import de.arnzel.legaladvice.LegalAdvice;
import de.arnzel.legaladvice.LegalAdviceRepository;

import de.arnzel.legaladvice.searchresults.LegalAdviceSearchResultsView;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


public class LegalAdviceSearchForm extends VerticalLayout {
  
  private Binder<LegalAdvice> binder = new Binder<>();
  
  private final LegalAdviceRepository legalAdviceRepository;
  
  public LegalAdviceSearchForm(LegalAdviceRepository legalAdviceRepository){
    this.legalAdviceRepository = legalAdviceRepository;
    add(getCityInput());
    add(getFreeLegalAidInput());
    add(getPaidLegalAidInput());
    add(new Label("Advice Languages"));
    add(getFirstLanguageInput());
    add(getSecondLanguageInput());
    add(getSearchButton());
    setJustifyContentMode(JustifyContentMode.BETWEEN);
  }

  private Component getCityInput(){
    List<String> possibleCities =
            legalAdviceRepository.findAllLegalAdviceCities();
    Select<String> citySelect =
            new Select<String>(possibleCities.toArray(new String[0]));
    citySelect.setPlaceholder("City");
    citySelect.setWidth("100%");

    binder.bind(citySelect,
        LegalAdvice::getCity,
        LegalAdvice::setCity);
    return citySelect;
  }
  
  private Component getFreeLegalAidInput(){
    Checkbox checkbox = new Checkbox();
    checkbox.setLabel("Free Legal Aid");
    checkbox.setWidth("100%");
    binder.bind(checkbox,
        LegalAdvice::isSupportsFreeLegalAid,
        LegalAdvice::setSupportsFreeLegalAid);
    return checkbox;
  }

  private Component getPaidLegalAidInput(){
    Checkbox checkbox = new Checkbox();
    checkbox.setLabel("Paid Legal Aid");
    checkbox.setWidth("100%");
    binder.bind(checkbox,
        LegalAdvice::isSupportsPaidLegalAid,
        LegalAdvice::setSupportsPaidLegalAid);
    return checkbox;
  }

  private Component getFirstLanguageInput(){
    List<String> possibleLanguages =
            legalAdviceRepository.findAllLegalAdviceLanguages();
;
    Select<String> languageSelect =
            new Select<String>(possibleLanguages.toArray(new String[0]));
    languageSelect.setPlaceholder("First Language");
    languageSelect.setWidth("100%");
    binder.bind(languageSelect,
        LegalAdvice::getPrimaryLanguage,
        LegalAdvice::setPrimaryLanguage);
    return languageSelect;
  }

  private Component getSecondLanguageInput(){
    List<String> possibleLanguages =
            legalAdviceRepository.findAllLegalAdviceLanguages();
    Select<String> languageSelect =
            new Select<String>(possibleLanguages.toArray(new String[0]));
    languageSelect.setPlaceholder("Second Language");
    languageSelect.setWidth("100%");
    binder
        .bind(languageSelect,
            LegalAdvice::getSecondaryLanguage,
            LegalAdvice::setSecondaryLanguage);
    return languageSelect;
  }


  private Component getSearchButton() {
    
    AsylumAdviceButton asylumAdviceButton = new AsylumAdviceButton("Search",event -> {
      try {
        LegalAdvice legalAdvice = new LegalAdvice();
        binder.writeBean(legalAdvice);
        List<LegalAdvice> legalAdviceList=
            legalAdviceRepository
                .findByCityByCityAndLanguages(
                        legalAdvice.getCity(),
                        legalAdvice.getPrimaryLanguage(),
                        legalAdvice.getSecondaryLanguage());
        setLegalAdviceSearchResults(legalAdviceList);
        getCurrent().navigate(LegalAdviceSearchResultsView.class);
      } catch (ValidationException e) {
        e.printStackTrace();
      }
    });
    return asylumAdviceButton;
  }

  
}
