package de.arnzel.legaladvice.search;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.router.Route;
import de.arnzel.views.AbstractMainView;
import de.arnzel.legaladvice.LegalAdviceRepository;
import org.springframework.beans.factory.annotation.Autowired;

@Route
public class LegalAdviceSearchView extends AbstractMainView {

  private final LegalAdviceSearchForm legalAdviceSearchForm;

  @Autowired
  public LegalAdviceSearchView(LegalAdviceRepository legalAdviceRepository){
    this.legalAdviceSearchForm = new LegalAdviceSearchForm(legalAdviceRepository);
  }
  
  @Override
  protected Component getContent() {
    return legalAdviceSearchForm;
  }
}
