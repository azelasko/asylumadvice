package de.arnzel.legaladvice;

import de.arnzel.views.question.Option;

import static com.vaadin.flow.server.VaadinSession.getCurrent;

import java.util.List;

public class LegalAdviceSessionHolder {

  public static final String LEGAL_ADVICES_SEARCH_RESULTS = "legalAdvicesSearchResult";

  public static final String LEGAL_ADVICES_SELECTED = "legalAdviceSelected";

  public static final String OPTIONS_SELECTED = "optionsSelected";


  public static List<LegalAdvice> getLegalAdviceSearchResults(){
    return (List<LegalAdvice>)getCurrent().getAttribute(LEGAL_ADVICES_SEARCH_RESULTS);
  }

  public static void setLegalAdviceSearchResults(List<LegalAdvice> legalAdvices){
    getCurrent().setAttribute(LEGAL_ADVICES_SEARCH_RESULTS,legalAdvices);
  }
  
  public static void setSelectedLegalAdvice(LegalAdvice legalAdvice){
    getCurrent().setAttribute(LEGAL_ADVICES_SELECTED,legalAdvice);
  }
  
  public static LegalAdvice getLegalAdviceSelected(){
    return (LegalAdvice)getCurrent().getAttribute(LEGAL_ADVICES_SELECTED);
  }

  public static void setSelectedOptions(List<Option> options){
    getCurrent().setAttribute(OPTIONS_SELECTED,options);
  }

  public static List<Option> getOptionsSelected(){
    return (List<Option>)getCurrent().getAttribute(OPTIONS_SELECTED);
  }
  
}
