package de.arnzel.legaladvice;

import static org.apache.commons.lang3.StringUtils.isEmpty;

import de.arnzel.legaladvice.searchresults.googlemaps.LatLon;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.validator.constraints.URL;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "LEGAL_ADVICE")
public class LegalAdvice {

  @Id
  @GeneratedValue(strategy= GenerationType.IDENTITY)
  @Column(name = "ID")
  private Long id;

  @Column(name = "NAME")
  @NotNull
  private String name;

  @Column(name = "STREET")
  @NotNull
  private String street;

  @Column(name= "STREET_NUMBER")
  private int streetNumber;

  @Column(name = "TELEPHONE_NUMBER")
  private String telephoneNumber;

  @Column(name = "EMAIL")
  @Email(message = "'${validatedValue}' is not a valid email")
  private String email;

  @Column(name = "HOMEPAGE")
  @URL(message = "'${validatedValue}' is not a valid URL")
  private String homepage;

  @Column(name = "OPENING_TIMES")
  private String openingTimes;

  @Column(name = "POSTAL_CODE")
  private String postalCode;

  @Column(name = "CITY")
  @NotNull
  private String city;

  @Column(name = "SUPPORT_FREE_LEGAL_AID")
  private boolean supportsFreeLegalAid;

  @Column(name = "SUPPORT_PAID_LEGAL_AID")
  private boolean supportsPaidLegalAid;

  @Column(name = "LATITUDE")
  private double latitude;

  @Column(name = "LONGTITUDE")
  private double longtitude;

  @ElementCollection(fetch = FetchType.EAGER)
  @CollectionTable(name = "LEGAL_ADVICE_LANGUAGES", joinColumns = @JoinColumn(name = "LEGAL_ADVICE_ID"))
  @Column(name = "LANGUAGE")
  private Set<String> languages = new HashSet<>();

  @Transient
  private String primaryLanguage;

  @Transient
  private String secondaryLanguage;

  public Long getId() {
    return id;
  }

  public String getPostalCode() {
    return postalCode;
  }

  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public double getLatitude() {
    return latitude;
  }

  public void setLatitude(double latitude) {
    this.latitude = latitude;
  }

  public double getLongtitude() {
    return longtitude;
  }

  public void setLongtitude(double longtitude) {
    this.longtitude = longtitude;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public int getStreetNumber() {
    return streetNumber;
  }

  public void setStreetNumber(int streetNumber) {
    this.streetNumber = streetNumber;
  }

  public String getTelephoneNumber() {
    return telephoneNumber;
  }

  public void setTelephoneNumber(String telephoneNumber) {
    this.telephoneNumber = telephoneNumber;
  }

  public String getHomepage() {
    return homepage;
  }

  public void setHomepage(String homepage) {
    this.homepage = homepage;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }
  
  public String getOpeningTimes() {
    return openingTimes;
  }

  public void setOpeningTimes(String openingTimes) {
    this.openingTimes = openingTimes;
  }

  public boolean isSupportsFreeLegalAid() {
    return supportsFreeLegalAid;
  }

  public void setSupportsFreeLegalAid(boolean supportsFreeLegalAid) {
    this.supportsFreeLegalAid = supportsFreeLegalAid;
  }

  public boolean isSupportsPaidLegalAid() {
    return supportsPaidLegalAid;
  }

  public void setSupportsPaidLegalAid(boolean supportsPaidLegalAid) {
    this.supportsPaidLegalAid = supportsPaidLegalAid;
  }
  
  public LatLon getLatLon(){
    return new LatLon(latitude,longtitude);
  }
  
  public String getFullAdress(){
    return street + " " +  streetNumber + "," + " " + postalCode + " " +  city;
  }

  public Set<String> getLanguages() {
    return languages;
  }

  public void setLanguages(Set<String> languages) {
    this.languages = languages;
  }

  public String getLanguagesString(){
    return String.join(",",languages);
  }

  public String getPrimaryLanguage() {
    return primaryLanguage;
  }

  public void setPrimaryLanguage(String primaryLanguage) {
    this.primaryLanguage = primaryLanguage;
  }

  public String getSecondaryLanguage() {
    return secondaryLanguage;
  }

  public void setSecondaryLanguage(String secondaryLanguage) {
    this.secondaryLanguage = secondaryLanguage;
  }

  @Override
  public String toString() {
    return new ReflectionToStringBuilder(this).toString();
  }


}
