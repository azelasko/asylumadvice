package de.arnzel.legaladvice.searchresults;

import static de.arnzel.Config.GOOGLE_MAPS_API_KEY;
import static de.arnzel.legaladvice.LegalAdviceSessionHolder.getLegalAdviceSearchResults;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.Route;
import de.arnzel.views.AbstractMainView;
import de.arnzel.legaladvice.LegalAdvice;
import de.arnzel.legaladvice.search.LegalAdviceSearchView;
import de.arnzel.legaladvice.searchresults.googlemaps.GoogleMapMarker;

import de.arnzel.legaladvice.searchresults.googlemaps.GoogleMapComponent;
import java.util.List;

@Route
public class LegalAdviceSearchResultsView extends AbstractMainView {


  @Override
  protected Component getContent() {
    HorizontalLayout horizontalLayout = new HorizontalLayout();
    GoogleMapComponent googleMapComponent =
        new GoogleMapComponent(GOOGLE_MAPS_API_KEY,null,null);
    googleMapComponent.setWidth("90vw");
    googleMapComponent.setHeight("90vh");
    List<LegalAdvice> legalAdvices =
        getLegalAdviceSearchResults();
    if(null == legalAdvices){
      //TODO: routing does not work
      UI.getCurrent().navigate(LegalAdviceSearchView.class);
    }
    if(!legalAdvices.isEmpty()){
      googleMapComponent.setCenter(legalAdvices.get(0).getLatLon());
      googleMapComponent.setZoom(15);
      addLegalAdviceMarkers(googleMapComponent,legalAdvices);
      horizontalLayout.add(googleMapComponent);
    }else{
      horizontalLayout.add(new Label("Unfornutately we didnt find any legal aid for the choosen parameters"));
    }
    return horizontalLayout;
  }
  
  private void addLegalAdviceMarkers(GoogleMapComponent googleMapComponent,
      List<LegalAdvice> legalAdvices){
   legalAdvices
        .stream()
        .forEach(legalAdvice -> {
          addLegalAdviceMarker(googleMapComponent,legalAdvice);
        });
  }
  
  private void addLegalAdviceMarker(GoogleMapComponent googleMapComponent,
      LegalAdvice legalAdvice){
    GoogleMapMarker googleMapMarker = new GoogleMapMarker(legalAdvice.getName(),
        legalAdvice.getLatLon(),
        false,"");
    Component infoWindowComponent = new LegalAdviceInfoWindowComponent(legalAdvice);
    googleMapMarker.addInfoWindow(infoWindowComponent);
    googleMapComponent.addMarker(googleMapMarker);
  }

}
