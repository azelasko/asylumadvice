package de.arnzel.legaladvice.searchresults;

import static com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment.START;
import static de.arnzel.legaladvice.LegalAdviceSessionHolder.setSelectedLegalAdvice;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.RouterLink;
import de.arnzel.legaladvice.LegalAdvice;
import de.arnzel.legaladvice.showSingle.ShowLegalAdviceView;

public class LegalAdviceInfoWindowComponent extends VerticalLayout {
  
  private LegalAdvice legalAdvice;
  
  public LegalAdviceInfoWindowComponent(LegalAdvice legalAdvice){
    this.legalAdvice = legalAdvice;
    add(getFirstRow());
    add(getSecondRow());
    add(getThirdRow());
  }


  private Component getFirstRow(){
    return new H2(legalAdvice.getName());
  }
  
  private Component getSecondRow(){
    HorizontalLayout layout = new HorizontalLayout();
    
    if(legalAdvice.isSupportsFreeLegalAid()){
      Label freeAdviceLabel = new Label("Costfree");
      layout.setVerticalComponentAlignment(START,
          freeAdviceLabel);
      layout.add(freeAdviceLabel);
    }

    if(legalAdvice.isSupportsPaidLegalAid()){
      Label paidAdviceLabel = new Label("Paid");
      layout.setVerticalComponentAlignment(START,
          paidAdviceLabel);
      layout.add(paidAdviceLabel);
    }
    return layout;
  }


  private Component getThirdRow() {

    RouterLink routerLink = new RouterLink("Details",ShowLegalAdviceView.class,legalAdvice.getId()){
      @Override
      public void afterNavigation(AfterNavigationEvent event) {
        super.afterNavigation(event);
        setSelectedLegalAdvice(legalAdvice);
      }
    };
    return routerLink;

  }

}
