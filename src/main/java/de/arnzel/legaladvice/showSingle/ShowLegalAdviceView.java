package de.arnzel.legaladvice.showSingle;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import de.arnzel.components.buttons.AsylumAdviceButtonRouting;
import de.arnzel.components.layouts.ExtendedVerticalLayout;
import de.arnzel.views.AbstractMainView;
import de.arnzel.legaladvice.LegalAdvice;
import de.arnzel.legaladvice.LegalAdviceRepository;
import de.arnzel.legaladvice.LegalAdviceInformationComponent;
import de.arnzel.views.overview.ShowQuestionsView;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;

@Route
public class ShowLegalAdviceView extends AbstractMainView implements HasUrlParameter<Long> {

  private final LegalAdviceRepository legalAdviceRepository;
  
  private Optional<LegalAdvice> legalAdviceOptional;
  
  private ExtendedVerticalLayout verticalLayout = new ExtendedVerticalLayout();
  
  @Autowired
  public ShowLegalAdviceView(LegalAdviceRepository legalAdviceRepository){
    this.legalAdviceRepository = legalAdviceRepository;
  }
  
  @Override
  protected Component getContent() {
    verticalLayout.setMargin(false);
    verticalLayout.setSpacing(false);
    verticalLayout.setPadding(false);
    return verticalLayout;
  }
  
  @Override
  public void setParameter(BeforeEvent event, Long id) {
    legalAdviceOptional = legalAdviceRepository.findById(id);
    if(legalAdviceOptional.isPresent()){
      LegalAdvice legalAdvice = legalAdviceOptional.get();
      verticalLayout.add(new LegalAdviceInformationComponent(legalAdvice));
      verticalLayout.add(new Hr());
      verticalLayout.add(new Paragraph("Before you contact your legal advisor go through our overview to be better prepared!"));
      verticalLayout.add(new AsylumAdviceButtonRouting("Start overview", ShowQuestionsView.class));
    }else {
      verticalLayout.add(new Label("The legal advice does not exists anymore"));
    }
  }
}
