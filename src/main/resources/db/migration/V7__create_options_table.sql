CREATE TABLE OPTIONS (
    ID INT AUTO_INCREMENT PRIMARY KEY,
    CODE VARCHAR(255),
    TEXT VARCHAR(255),
    QUESTION_ID INT NOT NULL,
    ASYLUM_INFORMATION_ID INT,
    FOREIGN KEY (QUESTION_ID) REFERENCES QUESTIONS(ID),
    FOREIGN KEY (ASYLUM_INFORMATION_ID) REFERENCES ASYLUM_INFORMATION(ID)
);