UPDATE ASYLUM_INFORMATION set EXPLANATION_TEXT_LONG  = '<h2>The Dublin Regulation</h2>

                                                        The Dublin-Regulation decides which EU Country is responsible for your asylum procedure. As a rule, the country that is responsible for your application is the one that you first entered or received a visa for.If, for example, you came to the EU through Greece via Turkey, then according to the Dublin-Regulation Greece is responsible for your asylum application. Because the situation for refugees in countries such as Greece or Italy can be quite difficult, many people seeking asylum travel to other countries after registering, such as Germany. It is possible that the police will find out if you have registered, given your fingerprints, or received a visa  in Greece. The Federal Office for Migration and Refugees (BAMF) would then begin the process of the so-called Dublin procedure.
                                                                                                                          <br>
                                                                                                                          <br>
                                                                                                                          <h2>What are the steps in the Dublin Regulation?</h2>
                                                                                                                          After your asylum application the Federal Office for Migration and Refugees (BAMF) will first determine if another country is responsible for your asylum application. In order to determine this you will be invited to a personal interview and asked about about your route to Europe.
                                                                                                                          <br>
                                                                                                                          <br>
                                                                                                                          When the Federal Office for Migration and Refugees (BAMF) determines that another country, not Germany, is responsible for your asylum application, then they will send you a letter saying that your request is not permissible. They will then deport you to the applicable country. This letter is often named the “Dublin Decision”. Once you have received this letter you have only a week to take action against this decision and file a lawsuit (see below).
                                                                                                                          <br>
                                                                                                                          <br>
                                                                                                                          The Federal Office for Migration and Refugees (BAMF) will then contact government agencies in other countries and request that they take over the proceedings for your asylum application. When the other country agrees, then the german authorities have 6 months to have you deported to that country. During this since month period you can actually be deported at any time.
                                                                                                                          <br>
                                                                                                                          <br>
                                                                                                                          When the Federal Office for Migration and Refugees (BAMF) decides that you are are in hiding not able to be found, the deadline of deportation can be extended to 18 months. One has to be careful here, because the Federal Office for Migration and Refugees (BAMF) will often claim that a person isn’t able to be found, even if the current whereabouts of the person is known, for example in the Church asylum (?). If this is the case in your situation you should ask a lawyer for help.
                                                                                                                          <br>
                                                                                                                          <br>
                                                                                                                          When you are not deported to another country by the deadline, then Germany is responsible for your asylum proceedings. That means that you are able to stay in Germany for at least the duration of the proceedings.
                                                                                                                          <br>
                                                                                                                          <br>
                                                                                                                          <h2>How can you fight against a Dublin decision?</h2>
                                                                                                                          When you have received a Dublin Decision that says your application was illegitimate, and that your deportation has been organized, you have to deal with it very quickly. You only have a week in order to file a lawsuit against the decision with the Administrative Court. It is best if you go there with a lawyer. You should also discuss with the lawyer if you should file a _____ in addition to the lawsuit. In the lawsuit and ………………. You have to clearly make your case to your lawyer as to why you absolutely cannot be deported to another country. For example, did something bad happen to you in this country? Or is the situation for refugees in this E.U. country (such as Greece or Italy)'
    WHERE CODE = 'DUBLIN_REGULATION';

    UPDATE ASYLUM_INFORMATION set EXPLANATION_TEXT_LONG  = '<h2>The Hearing (Anhörung)</h2>
                                                            The hearing for your application for asylum (Asylantrag) is the core of the asylum process (Asylverfahren). During the hearing you will present your reasons for your flight to the Federal Office for Migration and Refugees (BAMF) (Bundesamt für Migration und Flüchtlinge).It is very important here to recount everything from your situation in your home country, as well as to provide as much proof as possible for your story. In order to prepare yourself for this, you should absolutely seek legal counsel (Rechtsberatungsstelle) and receive personal advice.

                                                            <h2>Before the Hearing</h2>
                                                            You will receive an invitation to your hearing from the Federal Office for Migration and Refugees (BAMF) (Bundesamt für Migration und Flüchtlinge). Should you not be able to appear at the given appointment, it is very important that you inform BAMF of this in writing before the appointment. If you miss your appointment without an excuse, then BAMF can assume that you have rescinded your asylum application (Asylantrag) and order you to leave the country.
                                                            <br>
                                                            <br>
                                                            As the hearing is of such importance, you should prepare yourself for it very well. BAMF should offer you counsel for your asylum process (Asylverfahrensberatung) and inform you of your rights. This includes counselling with regards to the preparation for your hearing. Should BAMF not offer you counsel, then you should inquire about, and possibly request, it. You can otherwise seek out an independent legal counsel (Beratungsstelle) and ask for their advice, for example from charities (Wohlfahrtsverbänden).

                                                            <h2>Whom can I bring and who carries out the hearing?</h2>
                                                            The hearing itself will be carried about by an employee of the BAMF, who will be supported by a translator.
                                                            You can bring your own lawyer to the hearing, and, if you are not yet of legal age, your legal guardian as well.
                                                            <br>
                                                            <br>
                                                            It is also possible to bring further people as support, for friends. It is advisable to inform BAMF about any accompaniment, as it can otherwise result in misunderstandings or the unannounced persons being barred from the hearing.
                                                            <br>
                                                            <br>
                                                            Some people have a right to have their hearing be carried out about by a specially trained person. This is applicable to people who have a particular need for protection (besonders schutzbedürftige Personen), for example victims of human trafficking, sexual/gender-specific violence, genital mutilation, or torture, in addition to LGBTIQ* people, people suffering from trauma, and unaccompanied minors. If any of these conditions pertain to you, you should request a hearing with a specially trained person as early as possible before the hearing itself, ideally directly when submitting your application.

                                                            <h2>What happens during the Hearing?</h2>
                                                            Even if you can already speak German well, you should not waive your right to a translator. Many things can be explained more easily in one’s mother tongue as in a foreign language. Moreover, this offers you more time, which allows you to think a little longer about your answers.
                                                            <br>
                                                            <br>
                                                            At the beginning of the hearing you should make sure that you can understand your translator. If this is not the case, you can request for someone else to do the translation. You can also do this if you for any reason doubt the neutrality of your translator and do not believe that they are translating correctly.
                                                            <br>
                                                            <br>
                                                            In the hearing you will first talk about where you come from. You will probably receive many questions about the details of your country of origin or home town. The person conducting the hearing will also ask to describe the escape route you took to Germany in more detail. You will then be asked to specifically explain why you left your home country and why you cannot return there. Ideally, you would be able to support your reason for fleeing with proof (photos, documents, etc.).
                                                            <br>
                                                            <br>
                                                            The duration of the hearing is dependent upon the scope of your depiction and the intensity of the follow up questions posed by the person conducting it. It usually takes between one to three hours. You can request a pause at any time.
                                                            <br>
                                                            <br>
                                                            The entire content of the hearing will be recorded in a transcript. At the end of the hearing you can read the transcript yourself or have it read to you, translated into your own language. You will be asked to provide a signature for the transcript. It is important to only sign if everything you said was correctly written down. If something needs to be changed, then it is best to say this immediately, as you will probably not have another chance to amend things after the fact.
                                                            <br>
                                                            <br>
                                                            You will receive the transcript sent to your home in the mail. Keep it safe, as you may need it again later.
'
     WHERE CODE = 'ASYLUM_INTERVIEW';

     UPDATE ASYLUM_INFORMATION set EXPLANATION_TEXT_LONG  = '<h2>An Asylum Application with a Passport</h2>
                                                             As a citizen of a non-EU country you are required to possess a recognized and valid passport.
                                                             <br>
                                                             <br>
                                                             <h2>During the Asylum Process</h2>
                                                             If you do not possess a passport, then you are obligated to assist in procuring one. There is however no obligation during your asylum process (Asylverfahren) to go to the embassy of your country of origin in order to apply for a new passport. You cannot be expected to do this due to the possibility of the government of your country of origin persecuting you.

                                                             <h2>After the Asylum Process</h2>
                                                             There is still no obligation for you to procure a passport at your home country’s embassy after you have been recognized as a person entitled to asylum under the Germany constitution (Asylberechtigter nach dem Grundgesetz) or have been given status as a refugee. Instead, you will receive a blue passport (blauer Pass) from the Immigration Authority (Ausländerbehörde). It is very important that you do not contact the embassy of your country of origin, as you can lose your protected status as a refugee (Flüchtlingsschutz) by doing this.
                                                             <br>
                                                             <br>
                                                             As someone under subsidiary protection (subsidiär Geschützter), you can also apply for a gray passport, if you cannot be expected to contact your embassy in order to procure one.

                                                             <h2>By a Temporary Stay of Deportation</h2>
                                                             Should your application for asylum be rejected, it could still be essential that you possess a passport in order to apply for a temporary allowance to stay for education (Ausbildungsduldung) or hardship provision (Härtefallantrag). It is moreover still your obligation to cooperate in procuring a passport, as your financial assistance can otherwise be heavily cut.
'
     WHERE CODE = 'ASYLUM_WITHOUT_PASSPORT';
