
INSERT INTO ASYLUM_INFORMATION (TITLE,EXPLANATION_TEXT,EXPLANATION_TEXT_LONG,CODE)
    VALUES ('Asylum Procedure','','ASYLUM_PROCEDURE');

INSERT INTO ASYLUM_INFORMATION (TITLE,EXPLANATION_TEXT,EXPLANATION_TEXT_LONG,CODE)
    VALUES ('Forms of Asylum','If you apply for asylum in Germany successfully, you have the option of receiving one of four forms of protection. The rights you are granted with the respective form of protection are different','<h2>The Forms of Protection</h2>


                                                                                                                                                                                                                                    If you apply for asylum in Germany successfully, you have the option of receiving one of four forms of protection. The rights you are granted with the respective form of protection are different. The forms of protection are as follows:

                                                                                                                                                                                                                                    <ul>
                                                                                                                                                                                                                                      <li>Refugee status</li>
                                                                                                                                                                                                                                      <li>Asylum under Constitutional Law</li>
                                                                                                                                                                                                                                      <li>Subsidiary protection</li>
                                                                                                                                                                                                                                      <li>National bans on deportation</li>
                                                                                                                                                                                                                                    </ul>
                                                                                                                                                                                                                                    <br>
                                                                                                                                                                                                                                    Refugee status and asylum under the constitutional law grant you the most rights, while national deportation bans represent the lowest protection level. Accordingly, the forms of protection in your case are checked by the Federal Office for Migration and Refugees (BAMF) in the order listed.



                                                                                                                                                                                                                                    <h2>Refugee status</h2>


                                                                                                                                                                                                                                    You will be granted refugee status if you are outside of your home country due to justifiable fears of persecution based on your ‘race, religion, nationality, belonging to a specific social group or due to your political conviction’; and, you cannot return to your home country because you have no claims to state protection there, or you do not want to claim state protection for the aforementioned reasons.



                                                                                                                                                                                                                                    <h2>The 5 conditions for refugee status</h2>

                                                                                                                                                                                                                                    Refugee status hinges on the following five points and, in order to be recognized as a refugee, you have to be able to demonstrate the relevance of all five points in your case.
                                                                                                                                                                                                                                    <br>
                                                                                                                                                                                                                                    <br>

                                                                                                                                                                                                                                    <ul>

                                                                                                                                                                                                                                    <li>Persecution: You have been persecuted or have a reasonable fear of being persecuted. Massive violations of human rights, such as torture, slavery, or arbitrary punishment and killing are considered to be measures of persecution.
                                                                                                                                                                                                                                    It is also considered persecution when multiple discriminatory measures accumulate. This includes physical, psychological and sexual violence. Moreover, this is also the case if you are threatened by disproportionate and discriminatory law enforcement.
                                                                                                                                                                                                                                    </li>

                                                                                                                                                                                                                                    <li>Persecution Actors: In order to maintain refugee status, if you are persecuted, there must be certain people who have persecuted you. That is to say, people who are behind the actions of your persecution. These can be people who represent the state, but also individuals, militias, criminals and non-governmental groups. If the persecution does not come from the state, you must be able to prove that neither the state in which you live nor international organizations can offer you protection against the persecution.
                                                                                                                                                                                                                                     </li>
                                                                                                                                                                                                                                    <li>
                                                                                                                                                                                                                                    Persecution trait (feature?): In order to maintain refugee status, you have to have been persecuted based on a certain trait (“race”, religion, nationality, belonging to a certain social group or your political conviction).
                                                                                                                                                                                                                                      </li>
                                                                                                                                                                                                                                      <li>
                                                                                                                                                                                                                                    No availability of internal protection: In order to maintain refugee status, you have to prove that there is no region in your home country in which you could settle safely and without persecution.
                                                                                                                                                                                                                                      </li>
                                                                                                                                                                                                                                      <li>
                                                                                                                                                                                                                                    There are no grounds for exclusion: You are excluded from refugee protection if you have committed crimes that are contrary to international law, terrorist attacks, or other serious crimes which violate human rights. Refugee status can also be denied to you if you have a record of imprisonment of one or more years due to various acts of violence or sexual assault.
                                                                                                                                                                                                                                      </li>


                                                                                                                                                                                                                                    What rights do I have if I receive refugee status?
                                                                                                                                                                                                                                    Residence permit in Germany for three years
                                                                                                                                                                                                                                    A settlement permit can be issued after 5 years of continued refugee status. This period can be reduced to 3 years if a German language proficiency of level C1 is achieved, in combination with living expenses that are 71% of your monthly income.
                                                                                                                                                                                                                                    You can work without restrictions.
                                                                                                                                                                                                                                    You will receive a blue passport.
                                                                                                                                                                                                                                    You are entitled to privileged family reunification





                                                                                                                                                                                                                                    BOX examples:

                                                                                                                                                                                                                                    In his home country, Simon was detained and tortured by the police several times due to his political beliefs. People who share his political convictions are being persecuted by the police across the country. Subsequently, there is no way for him to settle safely in another area of ​​his home country. He also cannot rely on the protection of the state because it is the persecution actor. Simon has not committed any offenses which are listed in the exclusion criteria.

                                                                                                                                                                                                                                    Simon is entitled to refugee status because:

                                                                                                                                                                                                                                    Persecution: Torture
                                                                                                                                                                                                                                    Persecution Actors: police, state representation
                                                                                                                                                                                                                                    Persecution Trait: Political Belief
                                                                                                                                                                                                                                    No internal protection: the entire country is controlled by persecution actors
                                                                                                                                                                                                                                    No reasons for exclusion: No crimes committed according to the exclusion criteria


                                                                                                                                                                                                                                    Subsidiary protection

                                                                                                                                                                                                                                    Subsidiary protection acts as a supplement to refugee status. For a better understanding, we recommend that you first inform yourself about refugee status.

                                                                                                                                                                                                                                    For refugee status you have to be persecuted based on one or more characteristics (religion, political conviction ...). This is not the case for subsidiary protection. To receive subsidary protection, is sufficient if there is a risk of “serious damage”.


                                                                                                                                                                                                                                    The conditions for subsidiary protection

                                                                                                                                                                                                                                    You are considered to be at risk of serious harm if you are facing one of the following three dangers:
                                                                                                                                                                                                                                    Risk of torture or other inhumane treatment
                                                                                                                                                                                                                                    The danger of the death penalty
                                                                                                                                                                                                                                    The danger of falling victim to arbitrary violence in the context of an armed conflict

                                                                                                                                                                                                                                    This protection status often applies if you have fled a war zone but do not meet the requirements for refugee status.

                                                                                                                                                                                                                                    As with refugee status, a condition for subsidiary protection is that you cannot apply for internal protection in another region within your home country. The same terms of exclusion apply here as with refugee status.


                                                                                                                                                                                                                                    What rights do I have if I receive subsidiary protection?

                                                                                                                                                                                                                                    Residence permit for one year
                                                                                                                                                                                                                                    With reapplication: two more years at a time
                                                                                                                                                                                                                                    Residence permit possible after five years (including asylum procedure). Condition: secure livelihood and sufficient knowledge of German
                                                                                                                                                                                                                                    You can work without restrictions
                                                                                                                                                                                                                                    Family reunification is limited (more information under the family reunification section)

                                                                                                                                                                                                                                    Example:

                                                                                                                                                                                                                                    Sarah fled her country as the armed conflict between two militias in her region of the country grew worse. Shortly after a house in her village was set on fire and two people were killed, Sarah decided to flee. The state has largely lost control of the region in which Sarah lives. The only way to escape the militia struggles is to cross the border into another country. The part of their country where relative peace prevails cannot be reached without crossing the front line.

                                                                                                                                                                                                                                    Sarah has the right to subsidiary protection because:

                                                                                                                                                                                                                                    The danger of being the victim of arbitrary violence in the context of an armed conflict between the two militias in her region is evident and likely
                                                                                                                                                                                                                                    In addition, it cannot be ruled out that there is also a risk of torture, inhuman or degrading treatment




                                                                                                                                                                                                                                    Sarah has no right to refugee status because:

                                                                                                                                                                                                                                    The human rights violations she fears are not linked to any particular persecution feature


                                                                                                                                                                                                                                    Asylum under Constitutional Law

                                                                                                                                                                                                                                    Asylum under Constitutional Law is almost identical to refugee protection, but it is rarely granted. To get it, you must not have entered any other EU country on your trip to Germany. That means you must have traveled to Germany by air.

                                                                                                                                                                                                                                    The conditions for asylum under Constitutional Law

                                                                                                                                                                                                                                    You have to fulfill the following criteria in order to receive asylum according to the Basic Law:

                                                                                                                                                                                                                                    Persecution: You have suffered persecution, or you have a reasonable fear of suffering it. The main acts of persecution are massive violations of human rights, such as torture, slavery, arbitrary punishment or killing.
                                                                                                                                                                                                                                    It is also considered persecution when multiple discriminatory measures accumulate, or build on one another. This includes physical, psychological and sexual violence. This also applies if you are threatened by disproportionate and discriminatory law enforcement.

                                                                                                                                                                                                                                    State persecution: In order to receive asylum under Constitutional Law, you have to be able to prove that you are being persecuted directly by the state in your home country or at the instruction of the state by third parties, or that you have justified fear of it. In contrast to refugee protection, only state actors are considered here.

                                                                                                                                                                                                                                    Persecution feature: In order to receive asylum under Constitutional Law, you have to be persecuted based on a certain feature/trait (“race”, religion, nationality, belonging to a social group or your political conviction).

                                                                                                                                                                                                                                    No reason for exclusion: You are excluded from asylum under Constitutional Law if you have committed crimes that are contrary to international law, terrorist attacks, or other serious crimes which violate human rights. Asylum under Constitutional Law can also be denied if you have a record of imprisonment of one or more years as a result of commiting various acts of violence or sexual assault.





                                                                                                                                                                                                                                    What rights do I have if I receive asylum under Constitutional Law?

                                                                                                                                                                                                                                    Residence permit in Germany for three years
                                                                                                                                                                                                                                    A settlement permit can be issued after 5 years of continued refugee status. This period can be reduced to 3 years if a German language proficiency of level C1 is achieved, in combination with living expenses that are 71% of monthly income.
                                                                                                                                                                                                                                    You can work without restrictions
                                                                                                                                                                                                                                    You are entitled to privileged family reunification



                                                                                                                                                                                                                                    Prohibition of deportation / National bans on Deportation

                                                                                                                                                                                                                                    National prohibition on deportation applies to you if you do not receive refugee status, asylum under Constitutional Law or subsidiary protection, but you are nevertheless not able to return to your home country.

                                                                                                                                                                                                                                    The conditions for bans on deportation

                                                                                                                                                                                                                                    To be issued a ban on deportation, you must fulfill one of the following conditions:
                                                                                                                                                                                                                                    You are seriously ill and deportation would be life-threatening due to a lack of health care
                                                                                                                                                                                                                                    You are at risk of torture or catastrophic humanitarian conditions in the country you are being deported to


                                                                                                                                                                                                                                    Residence permit in Germany for at least one year
                                                                                                                                                                                                                                    repeated extension possible
                                                                                                                                                                                                                                    A settlement permit is possible after five years (the duration of the asylum procedure is taken into account) if other requirements, such as securing one's livelihood and sufficient knowledge of German, are met
                                                                                                                                                                                                                                    You can work with the permission of the immigration office
','FORMS_OF_ASYLUM');

INSERT INTO ASYLUM_INFORMATION (TITLE,EXPLANATION_TEXT,CODE)
    VALUES ('General Statistics','','GENERAL_STATISTICS');

