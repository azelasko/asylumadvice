INSERT INTO ASYLUM_INFORMATION (TITLE,EXPLANATION_TEXT,CODE)
    VALUES ('The Asylum Interview','The hearing for your application for asylum (Asylantrag) is the core of the asylum process (Asylverfahren). During the hearing you will present your reasons for your flight to the Federal Office for Migration and Refugees (BAMF) (Bundemmt fur Migration and Flüchtlinge).','ASYLUM_INTERVIEW');

INSERT INTO ASYLUM_INFORMATION (TITLE,EXPLANATION_TEXT,CODE)
    VALUES ('The Dublin Regulation','The Dublin-Regulation decides which EU Country is responsible for your asylum procedure. As a rule, the country that is responsible for your application is the one that you first entered or received a visa for.','DUBLIN_REGULATION');

INSERT INTO ASYLUM_INFORMATION (TITLE,EXPLANATION_TEXT,CODE)
    VALUES ('Having no Passport','Without a passport you will run into some difficulties with the German authorities, as it will be hard to proof where you are from.','ASYLUM_WITHOUT_PASSPORT');

INSERT INTO ASYLUM_INFORMATION (TITLE,EXPLANATION_TEXT,CODE)
    VALUES ('Your Nationality','So far this year 2019, 87,6 percent of the asylum applications from Syria were accepted.','YOUR_NATIONALITY');

INSERT INTO ASYLUM_INFORMATION (TITLE,EXPLANATION_TEXT,CODE)
    VALUES ('Arriving with Family or having relatives in Germany','Arriving as a family in Germany you should make sure to mention this as early as possible to avoid being seperated. Also if you have relatives in Germany.','ARRIVING_WITH_FAMILY');

INSERT INTO ASYLUM_INFORMATION (TITLE,EXPLANATION_TEXT,CODE)
    VALUES ('Family Reunification','Wanting to reunite with your family members you will have to get support on family reunification. This should be mentioned as early as possible.','FAMILY_REUNIFICATION');

INSERT INTO ASYLUM_INFORMATION (TITLE,EXPLANATION_TEXT,CODE)
    VALUES ('Legal Aid and representation','There are various places in Germany where you can get legal advice free of charge. However, legal advice centers cannot represent you in court For this you need to pay a lawyer. In many cases, however, the','LEGAL_AID');








