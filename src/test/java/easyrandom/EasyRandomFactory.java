package easyrandom;

import org.jeasy.random.EasyRandom;

public class EasyRandomFactory {

    public static EasyRandom createEasyRandom(){
        EasyRandom easyRandom = new EasyRandom();
        return easyRandom;
    }
}
