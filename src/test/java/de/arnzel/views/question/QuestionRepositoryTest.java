package de.arnzel.views.question;

import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Set;

import static easyrandom.EasyRandomFactory.createEasyRandom;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class QuestionRepositoryTest {

    @Autowired
    private QuestionRepository questionRepository;

    @Test
    public void findAll(){

        // given


        // when
        List<Question> questionList =
                questionRepository.findAll();

        // then
        assertThat(questionList.size()).isEqualTo(8);


    }

    @Test
    public void findAllWithOptions(){
        // given


        // when
        Set<Question> questionList =
                questionRepository.findAllWithOptions();

        // then
        assertThat(questionList.size()).isEqualTo(8);
        questionList
                .stream()
                .forEach(question -> {
                    assertThat(question.getOptions().isEmpty()).isFalse();
                });
    }

    private Question givenQuestion(){
        Question question = createEasyRandom().nextObject(Question.class);
        questionRepository.save(question);
        return question;
    }
}
